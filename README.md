Folders' Structure:

/UseCases folder contains all the models of the use cases.

/SizeOverhead folder contains all the models along with their generated source code which are used for the size overhead evaluation. The Original subfolder includes the models and code before instrumentation, and Refined subfolder includes models and code after instrumentation.


/PerformanceOverhead folder contains all the traces which were produced during the experiment. The Original subfolder includes traces by the replication system generated from the original model, and Refined subfolder includes traces by the replication system generated from the instrumented model
